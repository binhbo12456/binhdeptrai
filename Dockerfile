FROM centos:7/Users/galaxyplay/Desktop/test/ansible/Dockerfile

LABEL MAINTAINER="Team System Galaxyplay<binhdv@Galaxyplay.com.vn>"


RUN  set -x && yum install -y epel-release yum-utils \ 
    && yum update -y \ 
    && yum -y install ansible vim curl wget git 

#install sshd
RUN set -x \
    && yum install openssh-server openssh-clients -y \
    && yum install telnet-server telnet -y 

#install golang 
RUN set -x && wget https://golang.org/dl/go1.15.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.15.linux-amd64.tar.gz \
    && GOROOT=/usr/local/go \
    && GOPATH=/usr/local/go/download \
    && ln -s /usr/local/go/bin/go /usr/bin/go 

#install postgres 
RUN set -x && rpm -ivh https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm \
    &&  yum install mysql-server -y

EXPOSE 8080 3306