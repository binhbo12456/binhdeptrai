/*
 Navicat Premium Data Transfer

 Source Server         : user_ssh
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : user_ssh

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 03/11/2020 14:39:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Groupip
-- ----------------------------
DROP TABLE IF EXISTS `Groupip`;
CREATE TABLE `Groupip` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nameGroup` varchar(255) DEFAULT NULL,
  `ip` varchar(46) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of Groupip
-- ----------------------------
BEGIN;
INSERT INTO `Groupip` VALUES (8, 'user_name', 'abc', '2020-10-08 07:40:52');
INSERT INTO `Groupip` VALUES (11, 'user_name', 'abc', '2020-10-08 07:40:52');
INSERT INTO `Groupip` VALUES (12, 'user_name', 'abc', '2020-10-08 07:40:58');
IN
COMMIT;

-- ----------------------------
-- Table structure for Hosts
-- ----------------------------
DROP TABLE IF EXISTS `Hosts`;
CREATE TABLE `Hosts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `ip` varchar(46) DEFAULT NULL,
  `zone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `name` (`name`),
  KEY `id` (`id`),
  CONSTRAINT `Hosts_ibfk_1` FOREIGN KEY (`name`) REFERENCES `Username` (`name`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of Hosts
-- ----------------------------
BEGIN;
INSERT INTO `Hosts` VALUES (1, 'user_name', '10.10.24.138', '', '2020-10-08 04:12:44');
INSERT INTO `Hosts` VALUES (2, 'user_name', '', '', '2020-10-08 04:12:44');
INSERT INTO `Hosts` VALUES (3, 'user_name', '', '', '2020-10-08 04:12:44');
COMMIT;

-- ----------------------------
-- Table structure for Username
-- ----------------------------
DROP TABLE IF EXISTS `Username`;
CREATE TABLE `Username` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `sshkey` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of Username
-- ----------------------------
BEGIN;
INSERT INTO `Username` VALUES (7, 'user_name', 'SSH key', '2020-10-08 04:12:44');
INSERT INTO `Username` VALUES (11, 'user_name', 'SSH key', '2020-10-08 04:12:44');
INSERT INTO `Username` VALUES (6, 'user_name', 'SSH key', '2020-10-08 04:12:44');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
