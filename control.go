package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type (
	User struct {
		Id         int    `json:"id"`
		Name       string `json:"name"`
		Sshkey     string `json:"sshkey"`
		CreateDate string `json:"create_date"`
	}

	Hosts struct {
		Id         int      `json:"id"`
		Name       string   `json:"name"`
		Ip         []string `json:"ip"`
		Zone       string   `json:"zone"`
		CreateDate string   `json:""`
	}

	UserHost struct {
		Id     int      `json:"id"`
		Name   string   `json:"name" form:"name" query:"name"`
		Sshkey string   `json:"sshkey" form:"sshkey" query:"sshkey"`
		Ip     []string `json:"ip" form:"ip" query:"ip"`
		Zone   string   `json:"zone" form:"zone" query:"zone"`
	}

	AddUser struct {
		Name   string   `json:"name"`
		Sshkey string   `json:"sshkey"`
		IP     []string `json:"ip"`
	}

	AddGroup struct {
		Group string   `json:"group"`
		IP    []string `json:"ip"`
	}
)

const (
	PATH_MAIN  = "vars/main1.yml"
	PATH_HOSTS = "hosts"

	GET_LIST_USER = "SELECT * FROM Username ORDER BY id DESC;"

	GET_HOST_USER = "SELECT Username.id, Username.name, Username.sshkey FROM Username WHERE Username.name LIKE '" //name
	GET_LIST_HOST = "SELECT ip from Hosts WHERE name = '"                                                         //name

	INSERT_USER = "INSERT INTO Username(name,sshkey) value (?,?)"
	INSERT_HOST = "INSERT INTO Hosts(name, ip) value (?,?)"

	DISABLE_FOREIGN_KEY = "SET GLOBAL FOREIGN_KEY_CHECKS = 0;"
	ENABLE_FOREIGN_KEY  = "SET GLOBAL FOREIGN_KEY_CHECKS = 1;"

	DELETE_USER      = "DELETE Username.*, Hosts.* FROM Username JOIN Hosts ON Username.name = Hosts.name WHERE Username.name = '"
	CHECK_ROW_EXISTS = "SELECT name FROM Username WHERE name = '"

	CHECK_HOST_EXISTS = "SELECT ip FROM Hosts WHERE ip = ''"
	DELETE_HOSTS      = "DELETE Hosts.* FROM Username JOIN Hosts ON Username.name = Hosts.name WHERE Hosts.ip = '"

	// SELECT_CONFSSH        = "SELECT id, host, hostname, user, port, identityfile from confssh"
	// SELECT_SINGLE_CONFSSH = "SELECT id, host, hostname, user, port, identityfile from confssh where host = '"

	INSERT_GROUPIP   = "INSERT INTO Groupip(nameGroup, ip) VALUES (?,?)"
	SELECT_GROUPNAME = "SELECT DISTINCT nameGroup from Groupip"
	SELECT_GROUPIP   = "SELECT ip FROM Groupip WHERE nameGroup = '"
	CHECK_IP         = "SELECT ip FROM Groupip where ip = '"
	DELETE_ALL_GROUP = "DELETE FROM Groupip WHERE nameGroup = '"
)

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "admin"
	dbPass := "V123b!@1g23fgfg123"
	dbName := "user_ssh"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func RemoveDuplicates(s []string) ([]string, []string) {
	encountered := make(map[string]struct{})
	result := make([]string, 0)
	duplicate := make([]string, 0)
	for _, v := range s {
		if _, ok := encountered[v]; ok {
			duplicate = append(duplicate, v)
			continue
		} else {
			encountered[v] = struct{}{}
			result = append(result, v)
		}
	}
	return result, duplicate
}

// centos = bash
const ShellToUse = "bash"

// run command
func Shellout(command string) (string, string, error) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command(ShellToUse, "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return stdout.String(), stderr.String(), err
}

func process(t *template.Template, vars interface{}) string {
	var tmplBytes bytes.Buffer

	err := t.Execute(&tmplBytes, vars)
	if err != nil {
		panic(err)
	}
	return tmplBytes.String()
}
func ProcessString(str string, vars interface{}) string {
	tmpl, err := template.New("tmpl").Parse(str)

	if err != nil {
		panic(err)
	}
	return process(tmpl, vars)
}
func ProcessFile(fileName string, vars interface{}) string {
	tmpl, err := template.ParseFiles(fileName)

	if err != nil {
		panic(err)
	}
	return process(tmpl, vars)
}

func main() {

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/getlistuser", getListUser)
	e.GET("/gethostuser", gethostuser) ///gethostuser?name=binhdv
	e.POST("/adduser", addUser)
	// {
	// 	"name": "abc",
	// 	"sshkey": "ssh-rsa",
	// 	"Ip": [
	// 		"123",
	// 		"456"
	// 	]
	// }

	e.GET("/deleteuser", deleteuser)       ///deleteuser?name=binhdv
	e.GET("/deletehost", deletehostuser)   //deletehost?name=binhdv&ip=10.10.24.183
	e.GET("/ansibleadd", ansibleAllip)     ///ansibleadd?name=binhdv
	e.GET("/ansibleaddip", addSingleip)    ///ansibleaddip?name=binhdv&ip=10.10.24.138
	e.GET("/ansibledel", deletesingle)     ///ansibledel?name=binhdv&ip=10.10.24.138
	e.GET("/ansibledelall", ansibledelall) ///ansibledelall?name=binhdv
	e.GET("/addconfig", addconfgroup)      ///addconfig?ip=10.10.24.138
	e.POST("/addgroupip", addipgroup)      ///addgroupip
	//{
	//	"group": "deli",
	//	"ip": [
	//		"123",
	//		"456"
	//	]
	//}

	e.GET("/getgroupip", getgroupip)       //getgroupip?group=deli
	e.GET("/getgroup", getlistgroup_allip) ///getgroup
	e.GET("/deletegroup", delgroupip)      //deletegroup?group=deli&ip=all

	e.Logger.Fatal(e.Start(":8080"))
}

func getListUser(c echo.Context) error {
	var id int
	var name, sshkey, createDate string
	var listUser []User

	db := dbConn()
	defer db.Close()

	rows, err := db.Query(GET_LIST_USER)
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&id, &name, &sshkey, &createDate); err != nil {
			fmt.Println(err)
		}
		user := User{
			Id:         id,
			Name:       name,
			Sshkey:     sshkey,
			CreateDate: createDate,
		}
		listUser = append(listUser, user)
	}
	return c.JSON(http.StatusOK, listUser)
}

func gethostuser(c echo.Context) error {
	// id 	int `json:"id"`
	// Name       string `json:"name"`
	// Sshkey     string `json:"sshkey"`
	// ip         string `json:"ip"`
	// zone       string `json:"zone"`
	var list []UserHost
	var listip []string
	var id int
	var name, sshkey, ip string

	db := dbConn()
	defer db.Close()

	rname := strings.TrimSpace(c.QueryParam("name"))
	// fmt.Println("name " + rname)
	rows, err := db.Query(GET_HOST_USER + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&id, &name, &sshkey); err != nil {
			fmt.Println(err)
		}
	}
	rows, err = db.Query(GET_LIST_HOST + rname + "';")
	for rows.Next() {
		if err := rows.Scan(&ip); err != nil {
			fmt.Println(err)
		}
		listip = append(listip, ip)
	}

	user := UserHost{
		Id:     id,
		Name:   name,
		Sshkey: sshkey,
		Ip:     listip,
	}

	list = append(list, user)

	return c.JSON(http.StatusOK, list)
}

func addUser(c echo.Context) (err error) {

	// Name   string   `json:"name"`
	// Sshkey string   `json:"sshkey"`
	// IP     []string `json:"ip"`
	db := dbConn()
	defer db.Close()

	u := new(AddUser)
	if err = c.Bind(u); err != nil {
		return
	}
	insertuser, err := db.Prepare(INSERT_USER)
	inserthost, err := db.Prepare(INSERT_HOST)
	if err != nil {
		panic(err.Error())
	}
	insertuser.Exec(u.Name, u.Sshkey)

	for i := range u.IP {
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("insert Name: " + u.Name + " ip: " + u.IP[i])
		inserthost.Exec(u.Name, u.IP[i])
	}
	return c.JSON(http.StatusOK, "insert user success")
	// return c.JSON(http.StatusOK, "ok")
}

func deleteuser(c echo.Context) error {
	db := dbConn()
	rname := strings.TrimSpace(c.QueryParam("name"))
	//fmt.Println(rname)

	rows, err := db.Query(CHECK_ROW_EXISTS + rname + "';")
	if err != nil {
		fmt.Println(err.Error())
	}
	var name string
	for rows.Next() {
		if err := rows.Scan(&name); err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println(len(name))

	if len(name) > 1 {

		fmt.Println(db.Query(DISABLE_FOREIGN_KEY))

		delForm, err := db.Query(DELETE_USER + rname + "';")
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(delForm)
		log.Println("DELETE")

		fmt.Println(db.Query(ENABLE_FOREIGN_KEY))

		defer db.Close()
		return c.String(http.StatusOK, "delete User success")
	} else {
		defer db.Close()
		return c.String(http.StatusBadRequest, "user notfound")
	}

}

//deletehost?name=binhdv&ip=10.10.24.183
func deletehostuser(c echo.Context) error {
	var listipr []string
	var listip []string
	var ip string

	db := dbConn()
	rname := strings.TrimSpace(c.QueryParam("name"))
	ipr := strings.TrimSpace(c.QueryParam("ip"))

	listipr = strings.Split(ipr, ",")

	for i := range listipr {
		check, err := db.Query("select ip from Hosts join Username on Username.name = Hosts.name where Hosts.ip = '" + listipr[i] + "' and Username.name = '" + rname + "';")
		if err != nil {
			fmt.Println(err)
		}
		for check.Next() {
			if err := check.Scan(&ip); err != nil {
				fmt.Println(err)
			}
			listip = append(listip, ip)
		}
	}
	for i := range listip {
		delete, err := db.Query("DELETE Hosts.* FROM Username JOIN Hosts ON Username.name = Hosts.name WHERE Hosts.ip = '" + listip[i] + "' AND Hosts.name = '" + rname + "';")
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(delete)
	}

	defer db.Close()
	return c.String(http.StatusOK, "Delete Success")
}

func ansibleAllip(c echo.Context) error {
	db := dbConn()
	rname := strings.TrimSpace(c.QueryParam("name"))

	var id int
	var name, sshkey, ip string
	var listip []string

	data, err := db.Query(GET_HOST_USER + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for data.Next() {
		if err := data.Scan(&id, &name, &sshkey); err != nil {
			fmt.Println(err)
		}
	}
	ipdata, err := db.Query(GET_LIST_HOST + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for ipdata.Next() {
		if err := ipdata.Scan(&ip); err != nil {
			fmt.Println(err)
		}
		listip = append(listip, ip)
	}

	vars := make(map[string]interface{})
	vars["SSH_USER"] = name
	vars["SSH_KEY"] = sshkey
	vars["HOSTS"] = listip
	mainFILE, hostsfFILE := writefile(vars)
	addAnsible()
	defer db.Close()
	return c.String(http.StatusOK, "add user on hosts sucess"+mainFILE+"\n"+hostsfFILE)

}

func addSingleip(c echo.Context) error {
	db := dbConn()
	var listip []string
	var listiptrue []string
	var name, sshkey, id, ip string

	rname := strings.TrimSpace(c.QueryParam("name"))
	ipr := strings.TrimSpace(c.QueryParam("ip"))
	listip = strings.Split(ipr, ",")

	for i := range listip {
		query := "SELECT ip FROM Hosts where ip = '" + listip[i] + "' AND name = '" + rname + "';"
		fmt.Println(query)
		check, err := db.Query(query)
		if err != nil {
			fmt.Println(err)
		}
		for check.Next() {
			if err := check.Scan(&ip); err != nil {
				fmt.Println(err)
			}
			listiptrue = append(listiptrue, ip)
		}
	}
	username, err := db.Query(GET_HOST_USER + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for username.Next() {
		if err := username.Scan(&id, &name, &sshkey); err != nil {
			fmt.Println(err)
		}
	}

	vars := make(map[string]interface{})
	vars["SSH_USER"] = name
	vars["SSH_KEY"] = sshkey
	vars["HOSTS"] = listiptrue
	mainFILE, hostsfFILE := writefile(vars)
	// addAnsible()
	defer db.Close()
	return c.String(http.StatusOK, "add user on hosts sucess"+mainFILE+"\n"+hostsfFILE)
}

func addAnsible() {
	var app = "ansible-playbook -i hosts playbooks/deploy.yml" // run ansible
	out, errout, err := Shellout(app)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println(out)
	fmt.Println(errout)
}

func deletesingle(c echo.Context) error {
	db := dbConn()
	var listipr []string
	var listiptrue []string
	var name, sshkey, id, ip string
	rname := strings.TrimSpace(c.QueryParam("name"))
	ipr := strings.TrimSpace(c.QueryParam("ip"))

	listipr = strings.Split(ipr, ",")
	for i := range listipr {
		checkip, err := db.Query("SELECT ip FROM Hosts where ip = '" + listipr[i] + "' AND name = '" + rname + "'")
		if err != nil {
			fmt.Println(err)
		}
		for checkip.Next() {
			if err := checkip.Scan(&ip); err != nil {
				fmt.Println(err)
			}
			listiptrue = append(listiptrue, ip)
		}
	}
	username, err := db.Query(GET_HOST_USER + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for username.Next() {
		if err := username.Scan(&id, &name, &sshkey); err != nil {
			fmt.Println(err)
		}
	}
	vars := make(map[string]interface{})
	vars["SSH_USER"] = name
	vars["SSH_KEY"] = sshkey
	vars["HOSTS"] = listiptrue
	mainFILE, hostsfFILE := writefile(vars)
	delAnsible()
	defer db.Close()
	return c.String(http.StatusOK, "delete user success"+mainFILE+"\n"+hostsfFILE)
}

func ansibledelall(c echo.Context) error {
	db := dbConn()
	var listip []string
	var name, sshkey, id, ip string
	rname := strings.TrimSpace(c.QueryParam("name"))

	ipdata, err := db.Query(GET_LIST_HOST + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for ipdata.Next() {
		if err := ipdata.Scan(&ip); err != nil {
			fmt.Println(err)
		}
		listip = append(listip, ip)
	}
	userdata, err := db.Query(GET_HOST_USER + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for userdata.Next() {
		if err := userdata.Scan(&id, &name, &sshkey); err != nil {
			fmt.Println(err)
		}
	}

	vars := make(map[string]interface{})
	vars["SSH_USER"] = name
	vars["SSH_KEY"] = sshkey
	vars["HOSTS"] = listip
	mainFILE, hostsfFILE := writefile(vars)
	//delAnsible()
	defer db.Close()
	delAnsible()
	return c.String(http.StatusOK, "delete all success"+mainFILE+"\n"+hostsfFILE)
}
func delAnsible() {
	var app = "ansible-playbook -i hosts playbooks/remove_user.yml" // run ansible
	out, errout, err := Shellout(app)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println(out)
	// print output
	fmt.Println(errout)
}

func writefile(vars map[string]interface{}) (mainstring string, hoststring string) {

	mainFILE := ProcessFile("config/templates/main.tmpl", vars)    //inventory
	hostsfFILE := ProcessFile("config/templates/hosts.tmpl", vars) //hosts
	fmt.Println("\n main.yml \n " + mainFILE)
	fmt.Println("\n hosts \n " + hostsfFILE)
	file, err := os.Create(PATH_MAIN)
	if err != nil {
		fmt.Println(err)
	} else {
		file.WriteString(mainFILE)
		fmt.Println("write file main.yml done")
	}
	file.Close()
	file1, err := os.Create(PATH_HOSTS)
	if err != nil {
		fmt.Println(err)
	} else {
		file1.WriteString(hostsfFILE)
		fmt.Println("write file hotst done")
	}
	file1.Close()
	return mainFILE, hostsfFILE
}

///addconfig?ip=10.10.24.138
func addconfig(c echo.Context) error {

	var listip []string
	ip := strings.TrimSpace(c.QueryParam("ip"))

	listip = strings.Split(ip, ",")

	vars := make(map[string]interface{})
	vars["SSH_USER"] = ""
	vars["SSH_KEY"] = ""
	vars["HOSTS"] = listip

	mainFILE, hostsfFILE := writefile(vars)

	fmt.Println(mainFILE)
	fmt.Println(hostsfFILE)

	AnsibleConf()
	return c.String(http.StatusOK, "add config")
}

func AnsibleConf() {
	var app = "ansible-playbook -i hosts playbooks/add_config.yml" // run ansible
	out, errout, err := Shellout(app)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println(out)
	// print output
	fmt.Println(errout)
}

//////// add ip sysctl.conf
//addconfig?group=deli&ip=all
func addconfgroup(c echo.Context) error {
	var listip []string
	//var listiptrue []string

	var ip string
	db := dbConn()

	group := strings.TrimSpace(c.QueryParam("group"))
	ipr := strings.TrimSpace(c.QueryParam("ip"))
	ipr = strings.ToLower(ipr)

	fmt.Println("ipr" + ipr)
	check := false
	if ipr == "all" {
		fmt.Println("ipr = all")
		rows, err := db.Query(SELECT_GROUPIP + group + "';") //get list ip
		if err != nil {
			fmt.Println(err)
		}
		for rows.Next() {
			if err := rows.Scan(&ip); err != nil {
				fmt.Println(err)
			}
			listip = append(listip, ip)
			check = true
		}
	} else {
		fmt.Println("ipr = listip")
		var list []string

		list = strings.Split(ipr, ",")
		fmt.Println(list)
		for i := range list {
			checkip, err := db.Query(CHECK_IP + list[i] + "';") //check ip exist on db
			if err != nil {
				fmt.Println(err)
			}
			var ipcheck string
			for checkip.Next() {
				if err := checkip.Scan(&ipcheck); err != nil {
					fmt.Println(err)
				}
				if len(ipcheck) < 1 {
					fmt.Println("ip not found in DB " + list[i])
				} else {
					check = true
					listip = append(listip, ipcheck)
				}
			}
		}
	}
	if check {
		//fmt.Println(listip)
		result, _ := RemoveDuplicates(listip)
		fmt.Println(result)
		vars := make(map[string]interface{})
		vars["SSH_USER"] = ""
		vars["SSH_KEY"] = ""
		vars["HOSTS"] = result
		mainFile, hostFile := writefile(vars) //write
		listip = nil
		fmt.Println(mainFile)
		fmt.Println(hostFile)
	} else {
		return c.String(http.StatusBadRequest, "list ip not found in DB")
	}
	//run ansible
	defer db.Close()
	AnsibleConf()
	return c.String(http.StatusOK, "add config ok")
}

func addipgroup(c echo.Context) error {
	db := dbConn()
	//var listip []string
	insert, err := db.Prepare(INSERT_GROUPIP)
	if err != nil {
		fmt.Println(err)
	}
	u := new(AddGroup)
	if err := c.Bind(u); err != nil {
		return err
	}
	for i := range u.IP {
		insert.Exec(u.Group, u.IP[i])
	}
	defer db.Close()
	return c.String(http.StatusOK, "add group success")
}

func getlistgroup_allip(c echo.Context) error {
	db := dbConn()
	var group, ip string
	var nameIP = make(map[string][]string)
	var listgroup []AddGroup
	rows, err := db.Query(SELECT_GROUPNAME)
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&group); err != nil {
			fmt.Println(err)
		}
		groupip, err := db.Query(SELECT_GROUPIP + group + "';")
		if err != nil {
			fmt.Println(err)
		}
		for groupip.Next() {

			if err := groupip.Scan(&ip); err != nil {
				fmt.Println(err)
			}
			nameIP[group] = append(nameIP[group], ip)
		}
	}
	for key, value := range nameIP {
		list := AddGroup{
			Group: key,
			IP:    value,
		}
		listgroup = append(listgroup, list)
	}
	defer db.Close()
	return c.JSON(http.StatusOK, listgroup)
}

func getgroupip(c echo.Context) error {
	db := dbConn()
	var ip string
	var listip []string
	rname := strings.TrimSpace(c.QueryParam("group"))
	rows, err := db.Query(SELECT_GROUPIP + rname + "';")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		if err := rows.Scan(&ip); err != nil {
			fmt.Println(err)
		}
		listip = append(listip, ip)
	}
	groupip := AddGroup{
		Group: rname,
		IP:    listip,
	}
	defer db.Close()
	return c.JSON(http.StatusOK, groupip)
}

//deletegroup?group=deli?ip=all
func delgroupip(c echo.Context) error {
	db := dbConn()
	var list []string
	var listiptrue []string
	group := strings.TrimSpace(c.QueryParam("group"))
	ip := strings.TrimSpace(c.QueryParam("ip"))
	list = strings.Split(ip, ",")

	if ip == "all" {
		fmt.Println("delete all ip group")
		if _, err := db.Query(DELETE_ALL_GROUP + group + "';"); err != nil {
			fmt.Println(err)
		}
	} else {
		for i := range list {
			var ip string
			checkip, err := db.Query("SELECT ip FROM Groupip where ip = '" + list[i] + "' AND nameGroup = '" + group + "';")
			if err != nil {
				fmt.Println(err)
			}
			for checkip.Next() {
				if err := checkip.Scan(&ip); err != nil {
					fmt.Println(err)
				}
				if len(ip) < 1 {
					fmt.Println("ip not found in DB")
				} else {
					listiptrue = append(listiptrue, ip)
				}
			}
		}
		for i := range listiptrue {
			del, err := db.Query("DELETE FROM Groupip WHERE ip = '" + listiptrue[i] + "' AND nameGroup = '" + group + "'; ")
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println(del)
		}
	}
	defer db.Close()
	return c.String(http.StatusOK, "delete success")
}
