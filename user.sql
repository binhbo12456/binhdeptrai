Create database user_ssh;
use user_ssh;

CREATE TABLE Username (
    id INT(6) UNSIGNED AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    sshkey TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    primary key (name),
    key (id)
);

CREATE TABLE Groupip (
    id INT(6)  UNSIGNED AUTO_INCREMENT,
    nameGroup VARCHAR(255),
    ip VARCHAR(46),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    key (id)
);

CREATE TABLE Hosts (
	id INT(6)  UNSIGNED AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL,
    ip varchar(46), -- https://stackoverflow.com/a/20473371
    zone varchar(255),
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    foreign key (name) references Username(name) ON DELETE CASCADE,
    key (id)
);



INSERT INTO Username(name,sshkey) VALUES ("user_name","ssh key");
INSERT INTO Username(name,sshkey) VALUES ("user_name","ssh key");
INSERT INTO Username(name,sshkey) VALUES ("user_name","ssh key");

INSERT INTO Hosts(name, ip, zone) VALUES ("user_name", "ip", "");
INSERT INTO Hosts(name, ip, zone) VALUES ("user_name", "ip", "");
INSERT INTO Hosts(name, ip, zone) VALUES ("user_name", "ip", "");

