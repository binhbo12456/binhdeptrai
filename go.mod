module manage-user-ssh

go 1.15

require (
	github.com/coreos/etcd v3.3.25+incompatible // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/labstack/echo/v4 v4.1.17
	github.com/modern-go/reflect2 v1.0.1 // indirect
	go.etcd.io/etcd v3.3.25+incompatible
	golang.org/x/crypto v0.0.0-20201001193750-eb9a90e9f9cb // indirect
	golang.org/x/net v0.0.0-20200930145003-4acb6c075d10 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
)
